-- 14
-- predict n sn | n == sn = return True
--              | n < sn = do putStrLn "el numero ingresado es mayor"
--                            return False
--              | otherwise = do putStrLn "el numero ingresado es menor"
--                               return False

-- loop n = do putStrLn "Ingrese el numero secreto:"
--             sn <- getLine
--             b <- predict n (read sn :: Int)
--             if b then return 0 else loop n

-- main = do putStrLn "Ingrese el numero secreto:"
--           sn <- getLine
--           loop (read sn :: Int)

-- 15
-- data State = S Int Int Int Int Int

-- initState = S 5 4 3 2 1

-- printBoard s@(S a b c d e) = do putStr "1: "
--                                 printRow a
--                                 putStr "2: "
--                                 printRow b
--                                 putStr "3: "
--                                 printRow c
--                                 putStr "4: "
--                                 printRow d
--                                 putStr "5: "
--                                 printRow e
--                                 return s
--   where printRow 0 = putStr "\n"
--         printRow n = do putStr "*"
--                         printRow (n-1)

-- updateState (S a b c e d) 1 = S (a-1) b c e d
-- updateState (S a b c e d) 2 = S a (b-1) c e d
-- updateState (S a b c e d) 3 = S a b (c-1) e d
-- updateState (S a b c e d) 4 = S a b c (e-1) d
-- updateState (S a b c e d) 5 = S a b c e (d-1)

-- isFinish (S 0 0 0 0 0) = True
-- isFinish _ = False

-- loop s = do printBoard s
--             putStrLn "Elija de que fila desea remover: "
--             ns <- getLine
--             s' <- return $ updateState s (read ns :: Int)
--             b <- return $ isFinish s'
--             if b then return s' else loop s' 

-- main = loop initState
import Data.Char
import System.Environment
--16
uppercase [] = []
uppercase (c:s) = toUpper c : uppercase s

main = do args <- getArgs
          f <- readFile (args !! 0)
          writeFile (args !! 1) (uppercase f)