-- -- import Data.Sequence.Internal
-- -- data Tree a = Leaf a | Branch (Tree a) (Tree a) deriving Show

-- -- instance Functor Tree where
-- --   fmap f (Leaf a) = Leaf (f a)
-- --   fmap f (Branch l r) = Branch (fmap f l) (fmap f r)

-- -- -- numTree t = fst (mapTreeNro update t 0)
-- -- --   where update a n = (n, n+1) 
-- --         -- mapTreeNro u (Leaf a) i = let (b, i') = u a i in (Leaf b, i')
-- --         -- mapTreeNro u (Branch l r) i = let (l', i') = mapTreeNro u l i
-- --         --                                   (r', i'') = mapTreeNro u r i'
-- --         --                               in (Branch l' r', i'')

-- -- mapTreeM :: (a -> State s b) -> Tree a -> State s (Tree b)
-- -- mapTreeM f (Leaf a) = do b <- f a
-- --                          (return (Leaf b))
-- -- mapTreeM f (Branch l r) = do l' <- mapTreeM f l 
-- --                              r' <- (mapTreeM f r)
-- --                              (return (Branch l' r'))

-- -- numTree t = snd $ runState (do t' <- mapTreeM update t
-- --                                return t') 0
-- --   where update a = State (\s -> (s+1,s))                      

-- -- newtype Output w a = Out (a,w) deriving Show
-- -- instance Monoid w => Functor (Output w) where
-- --   fmap f (Out (a,w)) = Out (f a, w)
-- -- instance Monoid w => Applicative (Output w) where
-- --   pure x = Out(x, mempty)
-- -- instance Monoid w => Monad (Output w) where
-- --   return x = Out(x, mempty)
-- --   (Out (a,w)) >>= f = let (Out (a',w')) = f a in Out (a', mappend w w')

-- -- data A a = Con a | Sum (A a) (A a) | Mul (A a) (A a) | Res (A a) (A a) | Div (A a) (A a)

-- -- write w = Out ((), w)

-- -- eval (Con a) = do write ("El termino (Con " ++ sa ++ ") tiene valor " ++ sa ++ "\n")
-- --                   return a
-- --                   where sa = (show a :: String)
-- -- eval (Sum l r) = do l' <- eval l
-- --                     r' <- eval r 
-- --                     return (l' + r')
-- -- eval (Res l r) = do l' <- eval l
-- --                     r' <- eval r 
-- --                     return (l' - r')                   

-- sequence :: Monad m => [m a] -> m [a]
-- sequence [] = return []
-- sequence (x:xs) = do x' <- x
--                      xs' <- Main.sequence xs
--                      return (x':xs')

-- liftM :: Monad m => (a -> b) -> m a -> m b
-- liftM f x = do x' <- x
--                return (f x')                     

-- liftM2 :: Monad m => (a -> b -> c) -> m a -> m b -> m c 
-- liftM2 f x y = do x' <- x 
--                   y' <- y 
--                   return (f x' y')        

-- sequenceM :: Monad m => [m a] -> m [a]
-- sequenceM = foldr (Main.liftM2 (\ a b-> a : b )) (return [])

-- data Error er a = Raise er | Return a deriving Show
-- instance Functor (Error er) where
--   fmap f (Raise e) = Raise e
--   fmap f (Return a) = Return (f a)
-- instance Applicative (Error er) where
--   pure x = Return x
-- instance Monad (Error er) where 
--   return x = Return x
--   (Raise e) >>= _ = Raise e
--   (Return a) >>= f = f a

-- head (x:xs) = Return x 
-- head [] = Raise "empty list"

-- tail (x:xs) = Return xs
-- tail [] = Raise "empty list"

-- push x xs = Return (x:xs)
-- pop = Main.tail

-- data T = Con Int | Div T T 
-- newtype M s e a = M {runM :: s -> Error e (a,s)}

-- instance Functor (M s e) where
--   fmap f (M g) = M (\s -> case g s of
--                                Return (a,s') -> runM (M (\s-> Return (f a,s))) s'
--                                Raise e -> Raise e )
-- instance Applicative (M s e) where
--   pure x = M (\s-> Return (x,s))
-- instance Monad (M s e) where
--   return x = M (\s-> Return (x,s))
--   M g >>= f = M (\s -> case g s of
--                             Return (a,s') -> runM (f a) s'
--                             Raise e -> Raise e)

-- raise :: String -> M s String a
-- raise s = M (\_ -> Raise s)

-- modify :: (s -> s) -> M s e ()
-- modify f = M (\s-> Return ((), f s))

-- eval :: T -> M Int String Int 
-- eval (Con n) = return n 
-- eval (Div t1 t2) = do v1 <- eval t1 
--                       v2 <- eval t2 
--                       if v2 == 0 then raise "Error: division por cero"
--                                  else do modify (+1)
--                                          return (div v1 v2)

-- doEval t = runM (eval t) 0

-- data Cont r a = Cont ((a -> r) -> r) deriving Show
-- instance Functor (Cont r) where
--   fmap f (Cont g) = Cont (\h -> g (h.f))
-- instance Applicative (Cont r) where
--   pure x = Cont (\f -> f x)
-- instance Monad (Cont r) where 
--   return a = Cont (\f -> f a)
--   (Cont g) >>= f = Cont (\h -> g (\a -> let (Cont f') = f a in f' h))
  -- g :: ((a -> r) -> r)
  -- h ::  (b -> r)
  -- f :: a -> Cont ((b -> r) -> r)

data M m a = Mk (m (Maybe a))

returnM :: Monad m => a -> M m a
returnM a = Mk (return (return a))