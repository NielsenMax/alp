module Untyped where

import Common
import Control.Monad
import Data.List
import Data.Maybe

----------------------------------------------
-- Seccón 2
-- Ejercicio 2: Conversión a términos localmente sin nombres
----------------------------------------------

bind :: Name -> Term -> Term
bind = bind' 0
  where
    bind' :: Int -> Name -> Term -> Term
    bind' i s l@(Free s')
      | s == s' = Bound i
      | otherwise = l
    bind' i s (l :@: r) = bind' i s l :@: bind' i s r
    bind' i s (Lam l) = Lam $ bind' (i + 1) s l
    bind' i s l = l

conversion :: LamTerm -> Term
conversion (LVar s) = Free $ Global s
conversion (App l r) = conversion l :@: conversion r
conversion (Abs s l) = Lam $ bind (Global s) $ conversion l

-------------------------------
-- Sección 3
-------------------------------

vapp :: Value -> Value -> Value
vapp (VLam f) l = f l
vapp (VNeutral n) l = VNeutral $ NApp n l

eval :: NameEnv Value -> Term -> Value
eval e t = eval' t (e, [])

eval' :: Term -> (NameEnv Value, [Value]) -> Value
eval' (Bound ii) (_, lEnv) = lEnv !! ii
eval' (Free n) (gEnv, _) = case lookup n gEnv of
  Just v -> v
  Nothing -> VNeutral $ NFree n
eval' (l :@: r) e = vapp (eval' l e) (eval' r e)
eval' (Lam t) (gEnv, lEnv) = VLam $ \v -> eval' t (gEnv, v : lEnv)

--------------------------------
-- Sección 4 - Mostrando Valores
--------------------------------

quote :: Value -> Term
quote = quote' 0
  where
    quote' :: Int -> Value -> Term
    quote' i (VNeutral n) = quoteNeutral i n
    quote' i (VLam f) = Lam $ bind (Quote i) $ quote' (i + 1) $ f (VNeutral (NFree (Quote i)))
    quoteNeutral :: Int -> Neutral -> Term
    quoteNeutral i (NFree n) = Free n
    quoteNeutral i (NApp n v) = quoteNeutral i n :@: quote' i v
