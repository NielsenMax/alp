module Eval2
  ( eval,
    Env,
  )
where

import AST
import Control.Monad
  ( ap,
    liftM,
    (>=>),
  )
import qualified Data.Map.Strict as M
import Data.Maybe
import Data.Strict.Tuple
import Monads

-- Entornos
type Env = M.Map Variable Int

-- Entorno nulo
initEnv :: Env
initEnv = M.empty

-- Mónada estado, con manejo de errores
newtype StateError a = StateError {runStateError :: Env -> Either Error (Pair a Env)}

-- Para calmar al GHC
instance Functor StateError where
  fmap = liftM

instance Applicative StateError where
  pure = return
  (<*>) = ap

-- Ejercicio 2.a: Dar una instancia de Monad para StateError:
instance Monad StateError where
  return x = StateError (\s -> Right (x :!: s))
  m >>= f =
    StateError
      ( runStateError m
          >=> (\(v :!: s') -> runStateError (f v) s')
      )

-- Ejercicio 2.b: Dar una instancia de MonadError para StateError:
instance MonadError StateError where
  throw e = StateError (\_ -> Left e)

-- Ejercicio 2.c: Dar una instancia de MonadState para StateError:
instance MonadState StateError where
  lookfor v =
    StateError
      ( \s -> case M.lookup v s of
          Nothing -> Left UndefVar
          Just a -> Right $ a :!: s
      )
  update v i = StateError (\s -> Right $ () :!: M.insert v i s)

-- Ejercicio 2.d: Implementar el evaluador utilizando la monada StateError.
-- Evalua un programa en el estado nulo
eval :: Comm -> Either Error Env
eval p = runStateError (stepCommStar p) initEnv >>= \(v :!: s) -> Right s

-- Evalua multiples pasos de un comando, hasta alcanzar un Skip
stepCommStar :: (MonadState m, MonadError m) => Comm -> m ()
stepCommStar Skip = return ()
stepCommStar c = stepComm c >>= \c' -> stepCommStar c'

-- Evalua un paso de un comando
stepComm :: (MonadState m, MonadError m) => Comm -> m Comm
stepComm Skip = return Skip
stepComm (Let v e) = do
  e' <- evalExp e
  update v e'
  return Skip
stepComm (Seq c1 c2) = do
  stepCommStar c1
  return c2
stepComm (IfThenElse e c1 c2) = do
  b <- evalExp e
  if b then stepCommStar c1 else stepCommStar c2
  return Skip
stepComm c@(While e c') = do
  b <- evalExp e
  if b
    then do
      stepCommStar c'
      return c
    else return Skip

unwind :: (MonadState m, MonadError m) => (a -> a -> b) -> Exp a -> Exp a -> m b
unwind op l r = do
  l' <- evalExp l
  r' <- evalExp r
  return $ op l' r'

-- Evalua una expresion
evalExp :: (MonadState m, MonadError m) => Exp a -> m a
evalExp (Const n) = return n
evalExp (Var v) = lookfor v
evalExp (UMinus n) = do
  n' <- evalExp n
  return (- n')
evalExp (Plus l r) = unwind (+) l r
evalExp (Minus l r) = unwind (-) l r
evalExp (Times l r) = unwind (*) l r
evalExp (Div l r) = do
  l' <- evalExp l
  r' <- evalExp r
  if r' == 0 then return (div l' r') else throw DivByZero
evalExp BTrue = return True
evalExp BFalse = return False
evalExp (Lt l r) = unwind (<) l r
evalExp (Gt l r) = unwind (>) l r
evalExp (And l r) = unwind (&&) l r
evalExp (Or l r) = unwind (||) l r
evalExp (Not e) = do
  e' <- evalExp e
  return (not e')
evalExp (Eq l r) = unwind (==) l r
evalExp (NEq l r) = unwind (/=) l r
evalExp (EAssgn v e) = do
  e' <- evalExp e
  update v e'
  return e'
evalExp (ESeq l r) = unwind (const id) l r
