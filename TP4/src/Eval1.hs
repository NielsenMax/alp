module Eval1
  ( eval,
    Env,
  )
where

import AST
import AST (Comm (IfThenElse))
import Control.Monad
  ( ap,
    liftM,
  )
import qualified Data.Map.Strict as M
import Data.Maybe
import Data.Strict.Tuple
import Monads
import Text.ParserCombinators.ReadPrec (step)
import Prelude hiding
  ( fst,
    snd,
  )

-- Entornos
type Env = M.Map Variable Int

-- Entorno nulo
initEnv :: Env
initEnv = M.empty

-- Mónada estado
newtype State a = State {runState :: Env -> Pair a Env}

instance Monad State where
  return x = State (\s -> (x :!: s))
  m >>= f = State (\s -> let (v :!: s') = runState m s in runState (f v) s')

-- Para calmar al GHC
instance Functor State where
  fmap = liftM

instance Applicative State where
  pure = return
  (<*>) = ap

instance MonadState State where
  lookfor v = State (\s -> lookfor' v s :!: s)
    where
      lookfor' v s = fromJust $ M.lookup v s
  update v i = State (\s -> () :!: update' v i s) where update' = M.insert

-- Ejercicio 1.b: Implementar el evaluador utilizando la monada State

-- Evalua un programa en el estado nulo
eval :: Comm -> Env
eval p = snd (runState (stepCommStar p) initEnv)

-- Evalua multiples pasos de un comando, hasta alcanzar un Skip
stepCommStar :: MonadState m => Comm -> m ()
stepCommStar Skip = return ()
stepCommStar c = stepComm c >>= \c' -> stepCommStar c'

-- Evalua un paso de un comando
stepComm :: MonadState m => Comm -> m Comm
stepComm Skip = return Skip
stepComm (Let v e) = do
  e' <- evalExp e
  update v e'
  return Skip
stepComm (Seq c1 c2) = do
  stepCommStar c1
  return c2
stepComm (IfThenElse e c1 c2) = do
  b <- evalExp e
  if b then stepCommStar c1 else stepCommStar c2
  return Skip
stepComm c@(While e c') = do
  b <- evalExp e
  if b
    then do
      stepCommStar c'
      return c
    else return Skip

unwind :: MonadState m => (a -> a -> b) -> Exp a -> Exp a -> m b
unwind op l r = do
  l' <- evalExp l
  r' <- evalExp r
  return $ op l' r'

-- Evalua una expresion
evalExp :: MonadState m => Exp a -> m a
evalExp (Const n) = return n
evalExp (Var v) = lookfor v
evalExp (UMinus n) = do
  n' <- evalExp n
  return (- n')
evalExp (Plus l r) = unwind (+) l r
evalExp (Minus l r) = unwind (-) l r
evalExp (Times l r) = unwind (*) l r
evalExp (Div l r) = unwind div l r
evalExp BTrue = return True
evalExp BFalse = return False
evalExp (Lt l r) = unwind (<) l r
evalExp (Gt l r) = unwind (>) l r
evalExp (And l r) = unwind (&&) l r
evalExp (Or l r) = unwind (||) l r
evalExp (Not e) = do
  e' <- evalExp e
  return (not e')
evalExp (Eq l r) = unwind (==) l r
evalExp (NEq l r) = unwind (/=) l r
evalExp (EAssgn v e) = do
  e' <- evalExp e
  update v e'
  return e'
evalExp (ESeq l r) = unwind (const id) l r
