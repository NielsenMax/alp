module Eval3
  ( eval,
    Env,
  )
where

import AST
import Control.Monad
  ( ap,
    liftM,
  )
import qualified Data.Map.Strict as M
import Data.Maybe
import Data.Strict.Tuple
import Monads

-- Entornos
type Env = M.Map Variable Int

-- Entorno nulo
initEnv :: Env
initEnv = M.empty

-- Ejercicio 3.a: Proponer una nueva m\'onada que
-- lleve una traza de ejecución (además de manejar errores y estado).
-- y dar su instancia de mónada. Llamarla |StateErrorTrace|.
newtype StateErrorTrace a = StateErrorTrace {runStateErrorTrace :: Env -> Either Error (Pair a (Pair Env Trace))}

-- Recuerde agregar las siguientes instancias para calmar al GHC:
instance Functor StateErrorTrace where
  fmap = liftM

instance Applicative StateErrorTrace where
  pure = return
  (<*>) = ap

instance Monad StateErrorTrace where
  return x = StateErrorTrace (\s -> Right (x :!: (s :!: "")))
  m >>= f =
    StateErrorTrace
      ( \s -> do
          (v :!: (s' :!: w)) <- runStateErrorTrace m s
          (v' :!: (s'' :!: w')) <- runStateErrorTrace (f v) s'
          return $ v' :!: (s'' :!: w ++ w')
      )

-- Ejercicio 3.b: Resolver en Monad.hs

-- Ejercicio 3.c: Dar una instancia de MonadTrace para StateErrorTrace.
instance MonadTrace StateErrorTrace where
  addTrace w = StateErrorTrace (\s -> Right (() :!: (s :!: w)))

-- Ejercicio 3.d: Dar una instancia de MonadError para StateErrorTrace.
instance MonadError StateErrorTrace where
  throw e = StateErrorTrace (\_ -> Left e)

-- Ejercicio 3.e: Dar una instancia de MonadState para StateErrorTrace.
instance MonadState StateErrorTrace where
  lookfor v =
    StateErrorTrace
      ( \s -> case M.lookup v s of
          Nothing -> Left UndefVar
          Just a -> Right $ a :!: (s :!: "")
      )
  update v i = StateErrorTrace (\s -> Right $ () :!: (M.insert v i s :!: ""))

-- Ejercicio 3.f: Implementar el evaluador utilizando la monada StateErrorTrace.
-- Evalua un programa en el estado nulo

eval :: Comm -> Either Error (Env, Trace)
eval p = runStateErrorTrace (stepCommStar p) initEnv >>= (\(v :!: (s :!: w)) -> Right (s, w))

-- Evalua multiples pasos de un comando, hasta alcanzar un Skip
stepCommStar :: (MonadState m, MonadError m, MonadTrace m) => Comm -> m ()
stepCommStar Skip = return ()
stepCommStar c = stepComm c >>= \c' -> stepCommStar c'

-- Evalua un paso de un comando
stepComm :: (MonadState m, MonadError m, MonadTrace m) => Comm -> m Comm
stepComm Skip = return Skip
stepComm c@(Let v e) = do
  e' <- evalExp e
  update v e'
  addTrace $ v ++ " --> " ++ show e' ++ "\n"
  return Skip
stepComm (Seq c1 c2) = do
  stepCommStar c1
  return c2
stepComm (IfThenElse e c1 c2) = do
  b <- evalExp e
  addTrace $ show e ++ " --> " ++ show b ++ "\n"
  if b then stepCommStar c1 else stepCommStar c2
  return Skip
stepComm c@(While e c') = do
  b <- evalExp e
  if b
    then do
      stepCommStar c'
      return c
    else return Skip

unwind :: (MonadState m, MonadError m, MonadTrace m, Show b) => (a -> a -> b) -> Exp a -> Exp a -> m b
unwind op l r = do
  l' <- evalExp l
  r' <- evalExp r
  return $ op l' r'

-- Evalua una expresion
evalExp :: (MonadState m, MonadError m, MonadTrace m) => Exp a -> m a
evalExp (Const n) = return n
evalExp (Var v) = lookfor v
evalExp e@(UMinus n) = do
  n' <- evalExp n
  return (- n')
evalExp e@(Plus l r) = unwind (+) l r
evalExp e@(Minus l r) = unwind (-) l r
evalExp e@(Times l r) = unwind (*) l r
evalExp e@(Div l r) = do
  l' <- evalExp l
  r' <- evalExp r
  if r' == 0
    then return (div l' r')
    else throw DivByZero
evalExp BTrue = return True
evalExp BFalse = return False
evalExp e@(Lt l r) = unwind (<) l r
evalExp e@(Gt l r) = unwind (>) l r
evalExp e@(And l r) = unwind (&&) l r
evalExp e@(Or l r) = unwind (||) l r
evalExp (Not e) = do
  e' <- evalExp e
  return (not e')
evalExp e@(Eq l r) = unwind (==) l r
evalExp e@(NEq l r) = unwind (/=) l r
evalExp e@(EAssgn v e1) = do
  e' <- evalExp e1
  update v e'
  addTrace $ show e ++ " --> " ++ show e' ++ "\n"
  return e'
evalExp e@(ESeq l r) = unwind (const id) l r
