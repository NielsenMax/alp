# TP 3

## Ejercicio 1

La funcion `infer` devuelve un tipo `Either String Type` ya que la inferencia de tipos puede fallar si se encuentra con un tipado incorrecto. En ese caso se devuelve `Left String`. Si el tipado es exito se devuelve `Right String`.

El operador `>>=` _(bind)_, toma un either de string o de tipos, nos referiremos a este como resultado, y una funcion que toma tipos y devuelve un either de string o de tipos. Luego retorna un either de string o tipos.

_Bind_ se encarga de aplicar la funcion tomada al _resultado_ solo si este es un tipo, o sea `Right Type`. Si es un error, o sea `Left String` lo devuelve tal cual. Vale la pena mencionar que la funcion utlizada `either` toma dos funciones (`a -> c` y `b -> c`) y un `Either`, le aplicara al valor envuelto por el `Either` la primera si el `Either` es `Left` y la segunda si es `Right`.
