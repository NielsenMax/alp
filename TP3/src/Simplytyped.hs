module Simplytyped
  ( conversion,
    -- conversion a terminos localmente sin nombre
    eval,
    -- evaluador
    infer,
    -- inferidor de tipos
    quote, -- valores -> terminos
  )
where

import Common
import Data.List
import Data.Maybe
import PrettyPrinter
import Text.PrettyPrint.HughesPJ (render, text)
import Prelude hiding ((>>=))

-- conversion a términos localmente sin nombres
conversion :: LamTerm -> Term
conversion = conversion' []

conversion' :: [String] -> LamTerm -> Term
conversion' b (LVar n) = maybe (Free (Global n)) Bound (n `elemIndex` b)
conversion' b (LApp t u) = conversion' b t :@: conversion' b u
conversion' b (LAbs n t u) = Lam t (conversion' (n : b) u)
conversion' b (LLet n t u) = Let (conversion' b t) (conversion' (n : b) u)
conversion' b (LAs t tt) = As (conversion' b t) tt
conversion' b LUnit = Unit
conversion' b (LFst t) = Fst $ conversion' b t
conversion' b (LSnd t) = Snd $ conversion' b t
conversion' b (LPair t u) = Pair (conversion' b t) (conversion' b u)
conversion' b LZero = Zero
conversion' b (LSuc n) = Suc (conversion' b n)
conversion' b (LRec t u v) = Rec (conversion' b t) (conversion' b u) (conversion' b v)

-----------------------
--- eval
-----------------------

sub :: Int -> Term -> Term -> Term
sub i t (Bound j) | i == j = t
sub _ _ (Bound j) = Bound j
sub _ _ (Free n) = Free n
sub i t (u :@: v) = sub i t u :@: sub i t v
sub i t (Lam t' u) = Lam t' (sub (i + 1) t u)
sub i t (Let t' u) = Let (sub i t t') (sub (i + 1) t u)
sub i t (As t' tt) = As (sub i t t') tt
sub i t Unit = Unit
sub i t (Fst t') = Fst $ sub i t t'
sub i t (Snd t') = Snd $ sub i t t'
sub i t (Pair t' u) = Pair (sub i t t') (sub i t u)
sub i t Zero = Zero
sub i t (Suc n) = Suc (sub i t n)
sub i t (Rec t' u v) = Rec (sub i t t') (sub i t u) (sub i t v)

-- evaluador de términos
eval :: NameEnv Value Type -> Term -> Value
eval _ (Bound _) = error "variable ligada inesperada en eval"
eval e (Free n) = fst $ fromJust $ lookup n e
eval _ (Lam t u) = VLam t u
eval e (Lam _ u :@: Lam s v) = eval e (sub 0 (Lam s v) u)
eval e (Lam t u1 :@: u2) = let v2 = eval e u2 in eval e (sub 0 (quote v2) u1)
eval e (u :@: v) = case eval e u of
  VLam t u' -> eval e (Lam t u' :@: v)
  _ -> error "Error de tipo en run-time, verificar type checker"
eval e (Let t u) = let v = eval e t in eval e $ sub 0 (quote v) u
eval e (As t tt) = eval e t
eval e Unit = VUnit
eval e (Fst t) = case eval e t of
  VPair t' _ -> t'
  _ -> error "Error de tipo en run-time, verificar type checker"
eval e (Snd t) = case eval e t of
  VPair _ t' -> t'
  _ -> error "Error de tipo en run-time, verificar type checker"
eval e (Pair t u) =
  let t' = eval e t
      u' = eval e u
   in VPair t' u'
eval e Zero = VNum NZero
eval e (Suc n) = VNum $
  NSuc $ case eval e n of
    VNum n' -> n'
    _ -> error "Error de tipo en run-time, verificar type checker"
eval e (Rec t u Zero) = eval e t
eval e (Rec t u (Suc v)) = eval e ((u :@: Rec t u v) :@: v)
eval e (Rec t u v) = case eval e v of
  VNum v' -> eval e $ Rec t u $ quoteNval v'
  _ -> error "Error de tipo en run-time, verificar type checker"

-----------------------
--- quoting
-----------------------

quote :: Value -> Term
quote (VLam t f) = Lam t f
quote VUnit = Unit
quote (VPair t u) = Pair (quote t) (quote u)
quote (VNum n) = quoteNval n

quoteNval :: NumVal -> Term
quoteNval NZero = Zero
quoteNval (NSuc n) = Suc (quoteNval n)

----------------------
--- type checker
-----------------------

-- type checker
infer :: NameEnv Value Type -> Term -> Either String Type
infer = infer' []

-- definiciones auxiliares
ret :: Type -> Either String Type
ret = Right

err :: String -> Either String Type
err = Left

(>>=) ::
  Either String Type -> (Type -> Either String Type) -> Either String Type
(>>=) v f = either Left f v

-- fcs. de error

matchError :: Type -> Type -> Either String Type
matchError t1 t2 =
  err $
    "se esperaba "
      ++ render (printType t1)
      ++ ", pero "
      ++ render (printType t2)
      ++ " fue inferido."

notfunError :: Type -> Either String Type
notfunError t1 = err $ render (printType t1) ++ " no puede ser aplicado."

notfoundError :: Name -> Either String Type
notfoundError n = err $ show n ++ " no está definida."

notpairError :: Type -> Either String Type
notpairError t = err $ render (printType t) ++ " no es del tipo Pair."

infer' :: Context -> NameEnv Value Type -> Term -> Either String Type
infer' c _ (Bound i) = ret (c !! i)
infer' _ e (Free n) = case lookup n e of
  Nothing -> notfoundError n
  Just (_, t) -> ret t
infer' c e (t :@: u) =
  infer' c e t >>= \tt ->
    infer' c e u >>= \tu ->
      case tt of
        FunT t1 t2 -> if tu == t1 then ret t2 else matchError t1 tu
        _ -> notfunError tt
infer' c e (Lam t u) = infer' (t : c) e u >>= \tu -> ret $ FunT t tu
infer' c e (Let t u) =
  infer' c e t >>= \tt ->
    infer' (tt : c) e u
infer' c e (As t tt) =
  infer' c e t >>= \tt' ->
    if tt == tt' then ret tt else matchError tt tt'
infer' c e Unit = ret UnitT
infer' c e (Fst t) =
  infer' c e t >>= \tt ->
    case tt of
      PairT tf _ -> ret tf
      _ -> notpairError tt
infer' c e (Snd t) =
  infer' c e t >>= \tt ->
    case tt of
      PairT tf _ -> ret tf
      _ -> notpairError tt
infer' c e (Pair t u) =
  infer' c e t >>= \tt ->
    infer' c e u >>= \tu ->
      ret $ PairT tt tu
infer' c e Zero = ret NatT
infer' c e (Suc t) =
  infer' c e t >>= \tt ->
    case tt of
      NatT -> ret NatT
      _ -> matchError NatT tt
infer' c e (Rec t u v) =
  infer' c e t >>= \tt ->
    infer' c e u >>= \tu -> case tu of
      FunT tt (FunT NatT tt')
        | tt == tt' ->
          infer' c e v >>= \tv -> case tv of
            NatT -> ret tt
            _ -> matchError NatT tv
      _ -> matchError (FunT tt (FunT NatT tt)) tu

----------------------------------
