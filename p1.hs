import Parsing
import Data.Char
import Control.Monad
import Control.Applicative hiding (many)

orParent :: Parser a -> Parser a
orParent p = do char '('
                x <- p
                char ')'
                return x
              <|> p
-- Ejercicio 4
data Expr = Num Int | BinOp Op Expr Expr deriving Show

data Op = Add | Mul | Min | Div deriving Show

expr = do t <- term
          (do char '+'
              e <- expr
              return (BinOp Add t e)
           <|> (do char '-'
                   e <- expr
                   return (BinOp Min t e))
           <|> return t)

term = do f <- factor
          (do char '*'
              t <- term
              return (BinOp Mul f t)
           <|> do char '/'
                  t <- term
                  return (BinOp Div f t)
           <|> return f) 

factor = do d <- int
            return (Num d)
         <|> do char '('
                e <- expr
                char ')'
                return e
-- Ejercicio 5
data AN = N Int | A Char deriving Show
type ANL = [ AN ]

aux = do i <- int
         return (N i)
      <|> (do char '\''
              c <- letter
              char '\''
              return (A c))

listAn = do char '['
            xs <- sepBy aux (char ',')
            char ']'
            return xs
-- Ejercicio 6 
data Basetype = DInt | DChar | DFloat  deriving Show
type Hasktype = [ Basetype ]

aux2 = do string "Int"
          return DInt
       <|>(do string "Char"
              return DChar)
       <|>(do string "Float"
               return DFloat)
          
tipos = sepBy aux2 (string " -> ")
-- Ejercicio 7
data Hasktype2  = DInt2 | DChar2 | DFloat2 | Fun Hasktype Hasktype deriving Show

aux3 = do string "Int"
          return DInt2
       <|>(do string "Char"
              return DChar2)
       <|>(do string "Float"
               return DFloat2)
    
tipos2 = do a <- aux3
            string " -> "
            b <- tipos2
            return (Fun a b)
         <|> aux2

-- Ejercicio 9


