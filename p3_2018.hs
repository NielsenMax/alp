newtype WriterMaybe w a = WM { runWM :: (Maybe a,[w]) }
instance Functor (WriterMaybe w) where
  fmap f (WM (a,w)) = WM (fmap f a, w)
instance Applicative (WriterMaybe w) where
  pure a = WM (Just a, [])
instance Monad (WriterMaybe w) where 
  return a = WM (Just a, [])
  (WM (a,w)) >>= f = case a of
                          Nothing -> WM (Nothing, w)
                          Just x -> let (x',w') = runWM (f x) in WM (x', w++w')



tell w = WM (Just (), w)
fail = WM (Nothing, [])

data Result = Accepted | Rejected

aux p [] = do tell ("UNMATCHED PACKET " ++ p)
              return False

process p r re = do tell ("MATCHED "++p++" WITH RULE "++r )
                    case re of
                         Rejected -> do tell "RULE Rejected"
                                        return False
                         Accepted -> do tell "Rule accepted"
                                        return True
                      
aux p [] = do tell ("UNMATCHED PACKET " ++ p)
              Main.fail
aux p rs = mapM (\(r,re)-> process p r re) rs



a = [("a", Accepted), ("b", Accepted), ("c", Accepted)]
b = [("a", Rejected), ("b", Accepted), ("c", Accepted)]
c = [("a", Accepted), ("b", Rejected), ("c", Accepted)]
d = [("a", Accepted), ("b", Accepted), ("c", Rejected)]

