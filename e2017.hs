import Parsing
import Data.Char
import Control.Monad
import Control.Applicative hiding (many)

data ExpInt = Num Int | Add ExpInt ExpInt deriving Show
data ExpSt = Always ExpInt | Cons ExpInt ExpSt | Tail ExpSt | Ifz ExpInt ExpSt ExpSt | Head ExpSt deriving Show

parens p = do symbol "("
              x <- p
              symbol ")"
              return x
           <|> p

addP = do l <- expInt
          symbol "+"
          r <- expInt
          return (Add l r)

expInt = do n <- parens natural
            return (Num n)
         <|> parens addP

always = do symbol "always"
            n <- expInt
            return (Always n)

consP = do symbol "cons"
           symbol "("
           n <- expInt
           symbol ","
           ns <- expTs
           symbol ")"
           return (Cons n ns)

tailP = do symbol "tail"
           ns <- expTs
           return (Tail ns)

headP = do symbol "head"
           ns <- expTs
           return (Head ns)

ifP = do symbol "ifz"
         symbol "("
         c <- expInt
         symbol ","
         l <- expTs
         symbol ","
         r <- expTs
         symbol ")"
         return (Ifz c l r)

expTs = parens expTs <|> always <|> consP <|> tailP <|> headP <|> ifP
 
