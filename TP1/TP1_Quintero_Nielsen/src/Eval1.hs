module Eval1
  ( eval,
    State,
  )
where

import AST
import qualified Data.Map.Strict as M
import Data.Strict.Tuple

-- Estados
type State = M.Map Variable Int

-- Estado nulo
-- Completar la definición
initState :: State
initState = M.empty

-- Busca el valor de una variable en un estado
-- Completar la definición
lookfor :: Variable -> State -> Int
lookfor v s = s M.! v

-- Cambia el valor de una variable en un estado
-- Completar la definición
update :: Variable -> Int -> State -> State
update = M.insert

-- Evalua un programa en el estado nulo
eval :: Comm -> State
eval p = stepCommStar p initState

-- Evalua multiples pasos de un comnado en un estado,
-- hasta alcanzar un Skip
stepCommStar :: Comm -> State -> State
stepCommStar Skip s = s
stepCommStar c s = Data.Strict.Tuple.uncurry stepCommStar $ stepComm c s

-- Evalua un paso de un comando en un estado dado
-- Completar la definición
stepComm :: Comm -> State -> Pair Comm State
stepComm Skip s = Skip :!: s
stepComm (Let v e) s = Skip :!: update v (evalExp e s) s
stepComm (IfThenElse b cL cR) s = if evalExp b s then Skip :!: stepCommStar cL s else Skip :!: stepCommStar cR s
stepComm (While b c) s = if evalExp b s then While b c :!: stepCommStar c s else Skip :!: s
stepComm (Seq cL cR) s = cR :!: stepCommStar cL s

unwind :: (a -> a -> b) -> Exp a -> Exp a -> State -> b
unwind op l r s = op (evalExp l s) (evalExp r s)

-- Evalua una expresion
-- Completar la definición
evalExp :: Exp a -> State -> a
evalExp (Const n) s = n
evalExp (Var v) s = lookfor v s
evalExp (UMinus e) s = - (evalExp e s)
evalExp (Plus l r) s = unwind (+) l r s
evalExp (Minus l r) s = unwind (-) l r s
evalExp (Times l r) s = unwind (*) l r s
evalExp (Div l r) s = unwind div l r s
evalExp (ECond b l r) s = unwind (\l' r' -> if evalExp b s then l' else r') l r s
evalExp BTrue s = True
evalExp BFalse s = False
evalExp (Lt l r) s = unwind (<) l r s
evalExp (Gt l r) s = unwind (>) l r s
evalExp (And l r) s = unwind (&&) l r s
evalExp (Or l r) s = unwind (||) l r s
evalExp (Not e) s = not $ evalExp e s
evalExp (Eq l r) s = unwind (==) l r s
evalExp (NEq l r) s = unwind (/=) l r s
