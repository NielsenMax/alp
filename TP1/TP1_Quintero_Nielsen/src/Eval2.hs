module Eval2
  ( eval,
    State,
  )
where

import AST
import qualified Data.Map.Strict as M
import Data.Strict.Tuple

-- Estados
type State = M.Map Variable Int

-- Estado nulo
-- Completar la definición
initState :: State
initState = M.empty

-- Busca el valor de una variable en un estado
-- Completar la definición
lookfor :: Variable -> State -> Either Error Int
lookfor v s = case M.lookup v s of
  Nothing -> Left UndefVar
  Just a -> Right a

-- Cambia el valor de una variable en un estado
-- Completar la definición
update :: Variable -> Int -> State -> State
update = M.insert

-- Evalua un programa en el estado nulo
eval :: Comm -> Either Error State
eval p = stepCommStar p initState

-- Evalua multiples pasos de un comnado en un estado,
-- hasta alcanzar un Skip
stepCommStar :: Comm -> State -> Either Error State
stepCommStar Skip s = return s
stepCommStar c s = do
  (c' :!: s') <- stepComm c s
  stepCommStar c' s'

-- Evalua un paso de un comando en un estado dado
-- Completar la definición
stepComm :: Comm -> State -> Either Error (Pair Comm State)
stepComm Skip s = Right (Skip :!: s)
stepComm (Let v e) s = do
  n <- evalExp e s
  Right (Skip :!: update v n s)
stepComm (IfThenElse b cL cR) s = do
  n <- evalExp b s
  (Skip :!:) <$> if n then stepCommStar cL s else stepCommStar cR s
stepComm (While b c) s = do
  n <- evalExp b s
  ((if n then While b c else Skip) :!:) <$> if n then stepCommStar c s else Right s
stepComm (Seq cL cR) s = (cR :!:) <$> stepCommStar cL s

unwind :: (a -> a -> b) -> Exp a -> Exp a -> State -> Either Error b
unwind op l r s = do
  n <- evalExp l s
  op n <$> evalExp r s

-- Evalua una expresion
-- Completar la definición
evalExp :: Exp a -> State -> Either Error a
evalExp (Const n) s = Right n
evalExp (Var v) s = lookfor v s
evalExp (UMinus e) s = (* (-1)) <$> evalExp e s
evalExp (Plus l r) s = unwind (+) l r s
evalExp (Minus l r) s = unwind (-) l r s
evalExp (Times l r) s = unwind (*) l r s
evalExp (Div l r) s = do
  l' <- evalExp l s
  r' <- evalExp r s
  if r' == 0 then Left DivByZero else Right (div l' r')
evalExp (ECond b l r) s = do
  nb <- evalExp b s
  if nb then evalExp l s else evalExp r s
evalExp BTrue s = Right True
evalExp BFalse s = Right False
evalExp (Lt l r) s = unwind (<) l r s
evalExp (Gt l r) s = unwind (>) l r s
evalExp (And l r) s = unwind (&&) l r s
evalExp (Or l r) s = unwind (||) l r s
evalExp (Not e) s = not <$> evalExp e s
evalExp (Eq l r) s = unwind (==) l r s
evalExp (NEq l r) s = unwind (/=) l r s
