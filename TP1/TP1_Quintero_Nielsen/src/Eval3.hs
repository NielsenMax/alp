module Eval3
  ( eval,
    State,
  )
where

import AST
import qualified Data.Map.Strict as M
import Data.Strict.Tuple

-- Estados
type State = (M.Map Variable Int, Integer)

-- Estado nulo
-- Completar la definición
initState :: State
initState = (M.empty, 0)

-- Busca el valor de una variable en un estado
-- Completar la definición
lookfor :: Variable -> State -> Either Error Int
lookfor v (s, _) = case M.lookup v s of
  Nothing -> Left UndefVar
  Just a -> Right a

-- Cambia el valor de una variable en un estado
-- Completar la definición
update :: Variable -> Int -> State -> State
update k v (s, w) = (M.insert k v s, w)

-- Suma un costo dado al estado
-- Completar la definición
addWork :: Integer -> State -> State
addWork w (s, sw) = (s, sw + w)

-- Evalua un programa en el estado nulo
eval :: Comm -> Either Error State
eval p = stepCommStar p initState

-- Evalua multiples pasos de un comnado en un estado,
-- hasta alcanzar un Skip
stepCommStar :: Comm -> State -> Either Error State
stepCommStar Skip s = return s
stepCommStar c s = do
  (c' :!: s') <- stepComm c s
  stepCommStar c' s'

-- Evalua un paso de un comando en un estado dado
-- Completar la definición
stepComm :: Comm -> State -> Either Error (Pair Comm State)
stepComm Skip s = Right (Skip :!: s)
stepComm (Let v e) s = do
  n :!: ns <- evalExp e s
  Right (Skip :!: update v n ns)
stepComm (IfThenElse b cL cR) s = do
  n :!: s' <- evalExp b s
  (Skip :!:) <$> if n then stepCommStar cL s' else stepCommStar cR s'
stepComm (While b c) s = do
  n :!: s' <- evalExp b s
  ((if n then While b c else Skip) :!:) <$> if n then stepCommStar c s' else Right s'
stepComm (Seq cL cR) s = (cR :!:) <$> stepCommStar cL s

unwind :: (a -> a -> b) -> Integer -> Exp a -> Exp a -> State -> Either Error (Pair b State)
unwind op w l r s = do
  l' :!: s' <- evalExp l s
  r' :!: ns <- evalExp r s'
  Right (op l' r' :!: addWork w ns)

-- Evalua una expresion
-- Completar la definición
evalExp :: Exp a -> State -> Either Error (Pair a State)
evalExp (Const n) s = Right (n :!: s)
evalExp (Var v) s = (:!: s) <$> lookfor v s
evalExp (UMinus e) s = (\(n :!: s') -> (- n :!: addWork 1 s')) <$> evalExp e s
evalExp (Plus l r) s = unwind (+) 2 l r s
evalExp (Minus l r) s = unwind (-) 2 l r s
evalExp (Times l r) s = unwind (*) 3 l r s
evalExp (Div l r) s = do
  l' :!: s' <- evalExp l s
  r' :!: ns <- evalExp r s'
  if r' == 0 then Left DivByZero else Right (div l' r' :!: addWork 3 ns)
evalExp (ECond b l r) s = do
  nb :!: s' <- evalExp b s
  if nb then evalExp l s' else evalExp r s'
evalExp BTrue s = Right (True :!: s)
evalExp BFalse s = Right (False :!: s)
evalExp (Lt l r) s = unwind (<) 2 l r s
evalExp (Gt l r) s = unwind (>) 2 l r s
evalExp (And l r) s = unwind (&&) 2 l r s
evalExp (Or l r) s = unwind (||) 2 l r s
evalExp (Not e) s = (\(n :!: ns) -> not n :!: addWork 1 ns) <$> evalExp e s
evalExp (Eq l r) s = unwind (==) 2 l r s
evalExp (NEq l r) s = unwind (/=) 2 l r s
