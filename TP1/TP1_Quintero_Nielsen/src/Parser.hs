module Parser where

import AST
import Text.Parsec.Language (emptyDef)
import Text.Parsec.Token
import Text.ParserCombinators.Parsec

-----------------------
-- Funcion para facilitar el testing del parser.
totParser :: Parser a -> Parser a
totParser p = do
  whiteSpace lis
  t <- p
  eof
  return t

-- Analizador de Tokens
lis :: TokenParser u
lis =
  makeTokenParser
    ( emptyDef
        { commentStart = "/*",
          commentEnd = "*/",
          commentLine = "//",
          opLetter = char '=',
          reservedNames = ["true", "false", "if", "else", "while", "skip", "do"],
          reservedOpNames =
            [ "+",
              "-",
              "*",
              "/",
              "<",
              ">",
              "&&",
              "||",
              "!",
              "=",
              "==",
              "!=",
              ";",
              ",",
              "?",
              ":",
              "{",
              "}"
            ]
        }
    )

----------------------------------
--- Parser de expressiones enteras
-----------------------------------
iatom :: Parser (Exp Int)
iatom =
  try (parens lis intexp)
    <|> do
      i <- natural lis
      return (Const (fromIntegral i :: Int))
    <|> Var <$> identifier lis
    <|> do
      reservedOp lis "-"
      UMinus <$> iatom

factor :: Parser (Exp Int)
factor =
  iatom
    <|> do
      b <- batom
      reservedOp lis "?"
      eL <- intexp
      reservedOp lis ":"
      ECond b eL <$> intexp

addop :: Parser (Exp Int -> Exp Int -> Exp Int)
addop =
  do
    reservedOp lis "+"
    return Plus
    <|> do
      reservedOp lis "-"
      return Minus

mulop :: Parser (Exp Int -> Exp Int -> Exp Int)
mulop =
  do
    reservedOp lis "*"
    return Times
    <|> do
      reservedOp lis "/"
      return Div

term :: Parser (Exp Int)
term = chainl1 factor mulop

intexp :: Parser (Exp Int)
intexp = chainl1 term addop

-----------------------------------
--- Parser de expressiones booleanas
------------------------------------

batom :: Parser (Exp Bool)
batom =
  try (parens lis boolexp)
    <|> do
      reserved lis "true"
      return BTrue
    <|> do
      reserved lis "false"
      return BFalse
    <|> do
      reservedOp lis "!"
      Not <$> batom

boolFactor :: Parser (Exp Bool)
boolFactor =
  batom
    <|> do
      i <- intexp
      do
        reservedOp lis "<"
        Lt i <$> intexp
        <|> do
          reservedOp lis ">"
          Gt i <$> intexp
        <|> do
          reservedOp lis "=="
          Eq i <$> intexp
        <|> do
          reservedOp lis "!="
          NEq i <$> intexp

boolAnd :: Parser (Exp Bool -> Exp Bool -> Exp Bool)
boolAnd = do
  reservedOp lis "&&"
  return And

boolOr :: Parser (Exp Bool -> Exp Bool -> Exp Bool)
boolOr = do
  reservedOp lis "||"
  return Or

boolTerm :: Parser (Exp Bool)
boolTerm = chainl1 boolFactor boolAnd

boolexp :: Parser (Exp Bool)
boolexp = chainl1 boolTerm boolOr

-----------------------------------
--- Parser de comandos
-----------------------------------

commFactor :: Parser Comm
commFactor =
  do
    reserved lis "skip"
    return Skip
    <|> do
      v <- identifier lis
      reservedOp lis "="
      Let v <$> intexp
    <|> do
      reserved lis "if"
      b <- boolexp
      reservedOp lis "{"
      cL <- comm
      reservedOp lis "}"
      do
        reserved lis "else"
        reservedOp lis "{"
        cR <- comm
        reservedOp lis "}"
        return (IfThenElse b cL cR)
        <|> return (IfThen b cL)
    <|> do
      reserved lis "while"
      b <- boolexp
      reservedOp lis "{"
      c <- comm
      reservedOp lis "}"
      return (While b c)

commTerm :: Parser (Comm -> Comm -> Comm)
commTerm = do
  reservedOp lis ";"
  return Seq

comm :: Parser Comm
comm = chainl1 commFactor commTerm

------------------------------------
-- Función de parseo
------------------------------------
parseComm :: SourceName -> String -> Either ParseError Comm
parseComm = parse (totParser comm)
