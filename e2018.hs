import Parsing
import Data.Char
import Control.Monad
import Control.Applicative hiding (many)

brackets = do char '['
              xs <- sepBy int (char ',')
              char ']'
              return xs
cons = do x <- int
          char ':'
          xs <- cons
          return (x:xs)
       <|> brackets

listP = do x <- cons
           string "++"
           xs <- listP
           return (x++xs)
        <|> cons

